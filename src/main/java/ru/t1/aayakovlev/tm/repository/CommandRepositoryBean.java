package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.api.CommandRepository;
import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.model.Command;

public final class CommandRepositoryBean implements CommandRepository {

    private static final Command ABOUT = new Command(
            CommandConstant.ABOUT, ArgumentConstant.ABOUT,
            "Show developer info."
    );

    private static final Command INFO = new Command(
            CommandConstant.INFO, ArgumentConstant.INFO,
            "Show hardware info."
    );

    private static final Command HELP = new Command(
            CommandConstant.HELP, ArgumentConstant.HELP,
            "Show arguments description."
    );

    private static final Command VERSION = new Command(
            CommandConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command EXIT = new Command(
            CommandConstant.EXIT, null,
            "Exit program.");

    public static final Command[] COMMANDS = new Command[]{
            ABOUT, INFO, HELP, VERSION, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
