package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.api.CommandRepository;
import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.repository.CommandRepositoryBean;

import java.util.Scanner;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.EMPTY_ARRAY_SIZE;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT;
import static ru.t1.aayakovlev.tm.util.FormatUtil.format;

public class Application {

    private static final CommandRepository COMMAND_REPOSITORY = new CommandRepositoryBean();

    public static void main(String[] args) {
        processArguments(args);
        processCommand();
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return;
        String argument = arguments[FIRST_ARRAY_ELEMENT];
        processArgument(argument);
    }

    private static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;

        switch (argument) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.INFO:
                showInfo();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand() {
        System.out.println("**Welcome to Task Manager!**");
        Scanner scanner = new Scanner(System.in);
        String command;

        while(!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            command = scanner.nextLine();

            switch (command) {
                case CommandConstant.ABOUT:
                    showAbout();
                    break;
                case CommandConstant.VERSION:
                    showVersion();
                    break;
                case CommandConstant.HELP:
                    showHelp();
                    break;
                case CommandConstant.INFO:
                    showInfo();
                    break;
                case CommandConstant.EXIT:
                    showExit();
                default:
                    showCommandError();
                    break;
            }
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("email: aayakovlev@t1-consulting.ru");
        System.out.println("name: Yakovlev Anton.");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command: commands)
            System.out.println(command);
    }

    private static void showInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableCores = runtime.availableProcessors();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final boolean checkMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = checkMaxMemory ? "no limit" : format(maxMemory);
        final String freeMemoryFormat = format(freeMemory);
        final String totalMemoryFormat = format(totalMemory);
        final String usedMemoryFormat = format(usedMemory);

        System.out.println("[INFO]");
        System.out.println("Available cores: " + availableCores);
        System.out.println("Max memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Passed argument not recognized...");
        System.err.println("Try 'java -jar task-manager.jar -h' for more information.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Passed command not recognized...");
        System.err.println("Try 'help' for more information.");
    }

}
