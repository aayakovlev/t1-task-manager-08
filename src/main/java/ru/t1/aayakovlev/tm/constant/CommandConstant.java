package ru.t1.aayakovlev.tm.constant;

public final class CommandConstant {

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String VERSION = "version";

    public final static String EXIT = "exit";

    public final static String INFO = "info";

    private CommandConstant() {
    }

}
